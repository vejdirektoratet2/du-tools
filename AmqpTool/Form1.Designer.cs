﻿
namespace AmpqTool
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.tbClientId = new System.Windows.Forms.TextBox();
            this.tbPassword = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbSubscriptionUrl = new System.Windows.Forms.TextBox();
            this.btnStartListening = new System.Windows.Forms.Button();
            this.TbEventBody = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.TbEventProperties = new System.Windows.Forms.TextBox();
            this.btnStopListening = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.TbSendBody = new System.Windows.Forms.TextBox();
            this.BtnSendMessage = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.TbPropertiesToSend = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.TbSendQueueUrl = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.LabTokenExpiry = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 15);
            this.label1.TabIndex = 1;
            this.label1.Text = "ClientId";
            // 
            // tbClientId
            // 
            this.tbClientId.Location = new System.Drawing.Point(15, 36);
            this.tbClientId.Name = "tbClientId";
            this.tbClientId.Size = new System.Drawing.Size(228, 23);
            this.tbClientId.TabIndex = 2;
            // 
            // tbPassword
            // 
            this.tbPassword.Location = new System.Drawing.Point(265, 36);
            this.tbPassword.Name = "tbPassword";
            this.tbPassword.Size = new System.Drawing.Size(228, 23);
            this.tbPassword.TabIndex = 4;
            this.tbPassword.UseSystemPasswordChar = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(265, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 15);
            this.label2.TabIndex = 3;
            this.label2.Text = "Password";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tbPassword);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.tbClientId);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1572, 81);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Service Account";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(27, 143);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(138, 15);
            this.label3.TabIndex = 5;
            this.label3.Text = "Dataset subscription URL";
            // 
            // tbSubscriptionUrl
            // 
            this.tbSubscriptionUrl.Location = new System.Drawing.Point(27, 161);
            this.tbSubscriptionUrl.Name = "tbSubscriptionUrl";
            this.tbSubscriptionUrl.Size = new System.Drawing.Size(760, 23);
            this.tbSubscriptionUrl.TabIndex = 6;
            this.tbSubscriptionUrl.Text = "amqps://sb-duvdevdistribution.servicebus.windows.net/t-dataset-change-pwet490s-22" +
    "58/c2075044-cff5-4006-bb22-05fcbb9d8363";
            // 
            // btnStartListening
            // 
            this.btnStartListening.Location = new System.Drawing.Point(27, 206);
            this.btnStartListening.Name = "btnStartListening";
            this.btnStartListening.Size = new System.Drawing.Size(133, 23);
            this.btnStartListening.TabIndex = 7;
            this.btnStartListening.Text = "Start Listening";
            this.btnStartListening.UseVisualStyleBackColor = true;
            this.btnStartListening.Click += new System.EventHandler(this.btnConnectToSubscription_Click);
            // 
            // TbEventBody
            // 
            this.TbEventBody.Location = new System.Drawing.Point(19, 277);
            this.TbEventBody.Multiline = true;
            this.TbEventBody.Name = "TbEventBody";
            this.TbEventBody.Size = new System.Drawing.Size(707, 219);
            this.TbEventBody.TabIndex = 10;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.TbEventProperties);
            this.groupBox2.Controls.Add(this.TbEventBody);
            this.groupBox2.Location = new System.Drawing.Point(27, 244);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(748, 521);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Latest Received Event";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(19, 259);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(34, 15);
            this.label5.TabIndex = 12;
            this.label5.Text = "Body";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(19, 26);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 15);
            this.label4.TabIndex = 11;
            this.label4.Text = "Properties";
            // 
            // TbEventProperties
            // 
            this.TbEventProperties.Location = new System.Drawing.Point(19, 44);
            this.TbEventProperties.Multiline = true;
            this.TbEventProperties.Name = "TbEventProperties";
            this.TbEventProperties.Size = new System.Drawing.Size(707, 196);
            this.TbEventProperties.TabIndex = 9;
            // 
            // btnStopListening
            // 
            this.btnStopListening.Enabled = false;
            this.btnStopListening.Location = new System.Drawing.Point(181, 206);
            this.btnStopListening.Name = "btnStopListening";
            this.btnStopListening.Size = new System.Drawing.Size(133, 23);
            this.btnStopListening.TabIndex = 8;
            this.btnStopListening.Text = "Stop Listening";
            this.btnStopListening.UseVisualStyleBackColor = true;
            this.btnStopListening.Click += new System.EventHandler(this.btnStopListening_Click);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Location = new System.Drawing.Point(825, 99);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(4, 670);
            this.panel1.TabIndex = 14;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(19, 259);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(34, 15);
            this.label7.TabIndex = 14;
            this.label7.Text = "Body";
            // 
            // TbSendBody
            // 
            this.TbSendBody.Location = new System.Drawing.Point(19, 277);
            this.TbSendBody.Multiline = true;
            this.TbSendBody.Name = "TbSendBody";
            this.TbSendBody.Size = new System.Drawing.Size(652, 219);
            this.TbSendBody.TabIndex = 14;
            // 
            // BtnSendMessage
            // 
            this.BtnSendMessage.Location = new System.Drawing.Point(882, 206);
            this.BtnSendMessage.Name = "BtnSendMessage";
            this.BtnSendMessage.Size = new System.Drawing.Size(133, 23);
            this.BtnSendMessage.TabIndex = 12;
            this.BtnSendMessage.Text = "Send Message";
            this.BtnSendMessage.UseVisualStyleBackColor = true;
            this.BtnSendMessage.Click += new System.EventHandler(this.btnSendMessage_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(19, 29);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(60, 15);
            this.label8.TabIndex = 17;
            this.label8.Text = "Properties";
            // 
            // TbPropertiesToSend
            // 
            this.TbPropertiesToSend.Location = new System.Drawing.Point(19, 47);
            this.TbPropertiesToSend.Multiline = true;
            this.TbPropertiesToSend.Name = "TbPropertiesToSend";
            this.TbPropertiesToSend.Size = new System.Drawing.Size(652, 193);
            this.TbPropertiesToSend.TabIndex = 13;
            this.TbPropertiesToSend.Text = "exampleProperty1=exampleValue1\r\nexampleProperty2=exampleValue2\r\n";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(882, 143);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(141, 15);
            this.label9.TabIndex = 19;
            this.label9.Text = "Dataset ingest queue URL";
            // 
            // TbSendQueueUrl
            // 
            this.TbSendQueueUrl.Location = new System.Drawing.Point(882, 161);
            this.TbSendQueueUrl.Name = "TbSendQueueUrl";
            this.TbSendQueueUrl.Size = new System.Drawing.Size(702, 23);
            this.TbSendQueueUrl.TabIndex = 11;
            this.TbSendQueueUrl.Text = "amqps://sb-duvdevdistribution.servicebus.windows.net/q-something?";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label6.Location = new System.Drawing.Point(12, 111);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(161, 15);
            this.label6.TabIndex = 21;
            this.label6.Text = "Subscribe to dataset events";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label10.Location = new System.Drawing.Point(854, 111);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(251, 15);
            this.label10.TabIndex = 22;
            this.label10.Text = "Send events to dataset (for dataset owners)";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.TbSendBody);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.TbPropertiesToSend);
            this.groupBox3.Location = new System.Drawing.Point(882, 244);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(692, 521);
            this.groupBox3.TabIndex = 23;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Event To Send";
            // 
            // LabTokenExpiry
            // 
            this.LabTokenExpiry.AutoSize = true;
            this.LabTokenExpiry.Location = new System.Drawing.Point(320, 210);
            this.LabTokenExpiry.Name = "LabTokenExpiry";
            this.LabTokenExpiry.Size = new System.Drawing.Size(168, 15);
            this.LabTokenExpiry.TabIndex = 24;
            this.LabTokenExpiry.Text = "Token expires at [no token yet]";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1610, 779);
            this.Controls.Add(this.LabTokenExpiry);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.TbSendQueueUrl);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.BtnSendMessage);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnStopListening);
            this.Controls.Add(this.btnStartListening);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.tbSubscriptionUrl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "Form1";
            this.Text = "Dataudveksler AMQP Tool";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbClientId;
        private System.Windows.Forms.TextBox tbPassword;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbSubscriptionUrl;
        private System.Windows.Forms.Button btnStartListening;
        private System.Windows.Forms.TextBox TbEventBody;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox TbEventProperties;
        private System.Windows.Forms.Button btnStopListening;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox TbSendBody;
        private System.Windows.Forms.Button BtnSendMessage;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox TbPropertiesToSend;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox TbSendQueueUrl;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label LabTokenExpiry;
    }
}

