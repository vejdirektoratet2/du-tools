﻿using Amqp;
using Amqp.Framing;
using Amqp.Sasl;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using System.Windows.Forms;
using Message = Amqp.Message;

namespace AmpqTool
{
    // This example sends UTF8 and expects UTF8 in received messages.
    public partial class Form1 : Form
    {
        const string VdAzureTenantId = "f1044067-8c60-4022-98d8-69306c5f7238";

        private Connection _subscriptionConnection;
        private Session _subscriptionSession;
        private ReceiverLink _receiverLink;
        private static readonly HttpClient _httpClient = new();

        public Form1()
        {
            InitializeComponent();
            LoadUiValues();
        }

        void SaveUiValues()
        {
            SaveUiValue(tbClientId);
            SaveUiValue(tbSubscriptionUrl);
            SaveUiValue(TbSendQueueUrl);
            SaveUiValue(TbPropertiesToSend);
            SaveUiValue(TbSendBody);
        }

        void SaveUiValue(TextBox tb)
        {
            string tmpPath = Path.GetTempPath();
            File.WriteAllText(Path.Combine(tmpPath, tb.Name), tb.Text);
        }

        void LoadUiValues()
        {
            LoadUiValue(tbClientId);
            LoadUiValue(tbSubscriptionUrl);
            LoadUiValue(TbSendQueueUrl);
            LoadUiValue(TbPropertiesToSend);
            LoadUiValue(TbSendBody);
        }

        void LoadUiValue(TextBox tb)
        {
            string tmpPath = Path.GetTempPath();
            string filePath = Path.Combine(tmpPath, tb.Name);
            string value = File.Exists(filePath) ? File.ReadAllText(filePath) : string.Empty;
            tb.Text = string.IsNullOrWhiteSpace(value) ? tb.Text : value; // Keep existing example value if there is no value to load.
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            SaveUiValues();
        }

        void SplitSubscriptionAmqpUrl(string amqpUrl, out string subscriptionHost, out string subscriptionTopic, out string subscriptionId)
        {
            var uri = new Uri(amqpUrl);
            subscriptionHost = uri.Host;
            subscriptionTopic = uri.Segments[1].TrimEnd('/');
            subscriptionId = uri.Segments[2].TrimEnd('/');
        }

        private async void btnConnectToSubscription_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                btnStartListening.Enabled = false;
                await StartListening(tbClientId.Text.Trim(), tbPassword.Text.Trim(), tbSubscriptionUrl.Text.Trim(), VdAzureTenantId);
                btnStopListening.Enabled = true;
                Cursor = Cursors.Default;

                MessageBox.Show("You are now listening for events", "Success!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                MessageBox.Show(ex.Message, "Error starting AMQP listener", MessageBoxButtons.OK, MessageBoxIcon.Error);
                btnStartListening.Enabled = true;
            }
        }

        private async void btnStopListening_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                btnStopListening.Enabled = false;
                await StopListening();
                btnStartListening.Enabled = true;
                Cursor = Cursors.Default;
                MessageBox.Show("You have stopped listening for events", "Success!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                MessageBox.Show(ex.Message, "Error stopping AMQP listener", MessageBoxButtons.OK, MessageBoxIcon.Error);
                btnStopListening.Enabled = true;
            }
        }

        async Task StartListening(string clientId, string password, string amqpUrl, string tenantId)
        {
            SplitSubscriptionAmqpUrl(amqpUrl, out string subscriptionHost, out string subscriptionTopic, out string subscriptionId);

            var factory = new ConnectionFactory();
            factory.SASL.Profile = SaslProfile.Anonymous;

            var address = new Address(subscriptionHost, 5671, null, null, "/", "amqps");
            _subscriptionConnection = await factory.CreateAsync(address);

            string topicUrl = $"amqps://{subscriptionHost}/{subscriptionTopic}";
            string token = await PutTokenAsync(_subscriptionConnection, clientId, password, topicUrl, tenantId);
            var jwt = new JwtSecurityToken(token);
            LabTokenExpiry.Text = $"Token expires at {jwt.ValidTo.ToLocalTime()} (local time)";

            _subscriptionSession = new Session(_subscriptionConnection);
            _receiverLink = new ReceiverLink(_subscriptionSession, "ServiceBus.Cbs:receiver-link", subscriptionTopic + "/subscriptions/" + subscriptionId);
            _receiverLink.Start(int.MaxValue, OnMessageCallback);
        }

        async Task StopListening()
        {
            await _receiverLink.CloseAsync();
            await _subscriptionSession.CloseAsync();
            await _subscriptionConnection.CloseAsync();
        }

        void OnMessageCallback(IReceiverLink receiver, Message message)
        {
            var bytes = (byte[])message.Body;

            string body = bytes == null ? "<empty>" : Encoding.UTF8.GetString(bytes);

            this.Invoke((MethodInvoker)delegate () { TbEventBody.Text = body; });

            var sb = new StringBuilder();
            if (message.ApplicationProperties!= null)
            {
                sb.AppendLine("ApplicationProperties:");
                foreach (var prop in message.ApplicationProperties.Map)
                    sb.AppendLine($"    {prop.Key} = {prop.Value}");
            }

            if (message.MessageAnnotations != null)
            {
                sb.AppendLine();
                sb.AppendLine("MessageAnnotations:");
                foreach (var prop in message.MessageAnnotations.Map)
                    sb.AppendLine($"    {prop.Key} = {prop.Value}");
            }

            if (message.DeliveryAnnotations != null)
            {
                sb.AppendLine();
                sb.AppendLine("\nDeliveryAnnotations:");
                foreach (var prop in message.DeliveryAnnotations.Map)
                    sb.AppendLine($"{prop.Key} = {prop.Value}");
            }

            this.Invoke((MethodInvoker)delegate () { TbEventProperties.Text = sb.ToString(); });

            receiver.Accept(message);
        }

        private static async Task<string> PutTokenAsync(Connection connection, string clientId, string password, string topicUrl, string tenantId)
        {
            var session = new Session(connection);

            string cbsClientAddress = "http://localhost";
            var cbsSender = new SenderLink(session, "cbs-sender", "$cbs");
            var receiverAttach = new Attach()
            {
                Source = new Source() { Address = "$cbs" },
                Target = new Target() { Address = cbsClientAddress },
            };
            var cbsReceiver = new ReceiverLink(session, "cbs-receiver", receiverAttach, null);
            var token = await GetToken(clientId, password, tenantId);

            // construct the put-token message
            var request = new Message(token);
            request.Properties = new Properties();
            request.Properties.MessageId = "1";
            request.Properties.ReplyTo = cbsClientAddress;
            request.ApplicationProperties = new ApplicationProperties();
            request.ApplicationProperties["operation"] = "put-token";
            request.ApplicationProperties["type"] = "jwt";
            request.ApplicationProperties["name"] = topicUrl;
            await cbsSender.SendAsync(request);

            // receive the response
            var response = await cbsReceiver.ReceiveAsync();
            if (response == null || response.Properties == null || response.ApplicationProperties == null)
                throw new Exception("invalid response received");

            // validate message properties and status code.
            int statusCode = (int)response.ApplicationProperties["status-code"];
            if (statusCode != (int)HttpStatusCode.Accepted && statusCode != (int)HttpStatusCode.OK)
                throw new Exception("put-token message was not accepted. Error code: " + statusCode);

            // the sender/receiver may be kept open for refreshing tokens
            await cbsSender.CloseAsync();
            await cbsReceiver.CloseAsync();
            await session.CloseAsync();

            return token;
        }

        private static async Task<string> GetToken(string clientId, string password, string tenantId)
        {
            var requestContent = new FormUrlEncodedContent(new Dictionary<string, string>
            {
                { "grant_type", "client_credentials" },
                { "client_id", clientId },
                { "client_secret", password },
                { "scope", "https://servicebus.azure.net/.default" }
            });
            var requestUrl = $"https://login.microsoftonline.com/{tenantId}/oauth2/v2.0/token";

            var response = await _httpClient.PostAsync(requestUrl, requestContent);
            response.EnsureSuccessStatusCode();
            var responseContent = await response.Content.ReadAsStringAsync();
            var jsonDoc = JsonDocument.Parse(responseContent);
            var accessToken = jsonDoc.RootElement.GetProperty("access_token").GetString();
            return accessToken;
        }


        Dictionary<string, string> GetPropertiesToSend(string text)
        {
            var result = new Dictionary<string, string>();
            var lines = text.Trim().Split(Environment.NewLine, StringSplitOptions.RemoveEmptyEntries);

            foreach(var line in lines)
            {
                int splitPos = line.IndexOf('=');
                if (splitPos > 0)
                {
                    string key = line.Substring(0, splitPos).Trim();
                    string value = line.Substring(splitPos + 1).Trim();
                    result[key] = value;
                }
            }

            return result;
        }

        private async void btnSendMessage_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                BtnSendMessage.Enabled = false;
                await SendMessage(tbClientId.Text.Trim(), tbPassword.Text.Trim(), TbSendQueueUrl.Text.Trim(), VdAzureTenantId);
                BtnSendMessage.Enabled = true;
                Cursor = Cursors.Default;

                MessageBox.Show("Message sent", "Success!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                MessageBox.Show(ex.Message, "Error sending message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                BtnSendMessage.Enabled = true;
            }
        }

        async Task SendMessage(string clientId, string password, string queueUrl, string tenantId)
        {
            var propertiesToSend = GetPropertiesToSend(TbPropertiesToSend.Text);

            var uri = new Uri(queueUrl);
            string host = uri.Host;

            var factory = new ConnectionFactory();
            factory.SASL.Profile = SaslProfile.Anonymous;

            var address = new Address(host, 5671, null, null, "/", "amqps");
            var connection = await factory.CreateAsync(address);

            await PutTokenAsync(connection, clientId, password, queueUrl, tenantId);

            var session = new Session(connection);
            var senderLink = new SenderLink(session, "ServiceBus.Cbs:sender-link", queueUrl);

            // TODO: Different kinds of properties?
            var bodyBytes = Encoding.UTF8.GetBytes(TbSendBody.Text);
            var msg = new Message()
            {
                ApplicationProperties = new ApplicationProperties(),
                BodySection = new Data { Binary = bodyBytes }
            };
            foreach (var prop in propertiesToSend)
                msg.ApplicationProperties[prop.Key] = prop.Value;

            senderLink.Send(msg, TimeSpan.FromSeconds(10));
        }
    }
}
