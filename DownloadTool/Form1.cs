﻿using Microsoft.Net.Http.Headers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DownloadTool
{
    public partial class Form1 : Form
    {
        const string VdAzureTenantId = "f1044067-8c60-4022-98d8-69306c5f7238";
        HttpClient _httpClient = new HttpClient();

        public Form1()
        {
            InitializeComponent();
            LoadUiValues();
        }

        void SaveUiValues()
        {
            SaveUiValue(TbClientId);
            SaveUiValue(TbDownloadUrl);
        }

        void SaveUiValue(TextBox tb)
        {
            string tmpPath = Path.GetTempPath();
            File.WriteAllText(Path.Combine(tmpPath, tb.Name), tb.Text);
        }

        void LoadUiValues()
        {
            LoadUiValue(TbClientId);
            LoadUiValue(TbDownloadUrl);
        }

        void LoadUiValue(TextBox tb)
        {
            string tmpPath = Path.GetTempPath();
            string filePath = Path.Combine(tmpPath, tb.Name);
            string value = File.Exists(filePath) ? File.ReadAllText(filePath) : string.Empty;
            tb.Text = string.IsNullOrWhiteSpace(value) ? tb.Text : value; // Keep existing example value if there is no value to load.
        }

        class TokenResponse
        {
            public string access_token;
        }

        private async void BtnDownload_Click(object sender, EventArgs e)
        {
            try
            {
                TbActivityLog.Clear();

                Cursor = Cursors.WaitCursor;
                BtnDownload.Enabled = false;

                string authHeader;
                if (RadioOauth.Checked)
                {
                    LogMessage("Getting token from  Azure...");
                    string tokenResponse = await GetToken(TbClientId.Text.Trim(), TbPassword.Text.Trim(), VdAzureTenantId);
                    LogMessage($"Got token: {tokenResponse}");
                    var parsed = JsonConvert.DeserializeObject<TokenResponse>(tokenResponse);
                    string token = parsed.access_token;
                    authHeader = $"Bearer {token}";
                }
                else
                {
                    string basicAuth = $"{TbClientId.Text.Trim()}:{TbPassword.Text.Trim()}";
                    authHeader = $"Basic {Convert.ToBase64String(Encoding.UTF8.GetBytes(basicAuth))}";
                    LogMessage($"Using basic authentication: '{authHeader}'");
                }

                var saveDialog = new SaveFileDialog();
                var saveResult = saveDialog.ShowDialog();
                if (saveResult != DialogResult.OK)
                    return;

                LogMessage($"Downloading file to {saveDialog.FileName}...");
                await DownloadFile(TbDownloadUrl.Text.Trim(), authHeader, saveDialog.FileName);
                LogMessage("File downloaded");

                BtnDownload.Enabled = true;
                Cursor = Cursors.Default;

                MessageBox.Show("File downloaded", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                LogMessage("");
                LogMessage(ex.Message);
                MessageBox.Show(ex.Message, "Error downloading file", MessageBoxButtons.OK, MessageBoxIcon.Error);
                BtnDownload.Enabled = true;
            }
        }

        void LogMessage(string message)
        {
            TbActivityLog.Text += message + Environment.NewLine;
        }

        async Task DownloadFile(string url, string authHeader, string savePath)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            request.Headers.Add(HeaderNames.Authorization, authHeader);

            var response = await _httpClient.SendAsync(request);
            if (!response.IsSuccessStatusCode)
            {
                string responseBody = await response.Content.ReadAsStringAsync();
                throw new HttpRequestException($"Error from server: {(int)response.StatusCode}: {responseBody}");
            }

            using var downloadStream = await response.Content.ReadAsStreamAsync();
            using var fileStream = new FileStream(savePath, FileMode.OpenOrCreate);
            await downloadStream.CopyToAsync(fileStream);
        }

        async Task<string> GetToken(string clientId, string password, string tenantId)
        {
            string tokenUrl = $"https://login.microsoftonline.com/{tenantId}/oauth2/v2.0/token";

            var formParamaters = new Dictionary<string, string>
            {
                { "client_id", clientId },
                { "scope", $"{clientId}/.default" },
                { "client_secret", password },
                { "grant_type", "client_credentials" },
            };

            var response = await _httpClient.PostAsync(tokenUrl, new FormUrlEncodedContent(formParamaters));
            string responseBody = await response.Content.ReadAsStringAsync();
            if (!response.IsSuccessStatusCode)
                throw new HttpRequestException($"{response.StatusCode}: {responseBody}");

            return responseBody;
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            SaveUiValues();
        }
    }
}
