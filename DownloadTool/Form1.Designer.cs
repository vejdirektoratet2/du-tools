﻿
namespace DownloadTool
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.TbPassword = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.TbClientId = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.TbDownloadUrl = new System.Windows.Forms.TextBox();
            this.BtnDownload = new System.Windows.Forms.Button();
            this.TbActivityLog = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.RadioOauth = new System.Windows.Forms.RadioButton();
            this.RadioBasicAuth = new System.Windows.Forms.RadioButton();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.RadioBasicAuth);
            this.groupBox1.Controls.Add(this.RadioOauth);
            this.groupBox1.Controls.Add(this.TbPassword);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.TbClientId);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(776, 81);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Service Account";
            // 
            // TbPassword
            // 
            this.TbPassword.Location = new System.Drawing.Point(276, 37);
            this.TbPassword.Name = "TbPassword";
            this.TbPassword.Size = new System.Drawing.Size(228, 23);
            this.TbPassword.TabIndex = 2;
            this.TbPassword.UseSystemPasswordChar = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(276, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 15);
            this.label2.TabIndex = 5;
            this.label2.Text = "Password";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(141, 15);
            this.label1.TabIndex = 3;
            this.label1.Text = "ClientId / Basic Auth User";
            // 
            // TbClientId
            // 
            this.TbClientId.Location = new System.Drawing.Point(21, 37);
            this.TbClientId.Name = "TbClientId";
            this.TbClientId.Size = new System.Drawing.Size(228, 23);
            this.TbClientId.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 112);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 15);
            this.label3.TabIndex = 7;
            this.label3.Text = "Dataset URL";
            // 
            // TbDownloadUrl
            // 
            this.TbDownloadUrl.Location = new System.Drawing.Point(12, 130);
            this.TbDownloadUrl.Name = "TbDownloadUrl";
            this.TbDownloadUrl.Size = new System.Drawing.Size(760, 23);
            this.TbDownloadUrl.TabIndex = 4;
            this.TbDownloadUrl.Text = "https://www.abc.dk/api/dataset/123";
            // 
            // BtnDownload
            // 
            this.BtnDownload.Location = new System.Drawing.Point(12, 159);
            this.BtnDownload.Name = "BtnDownload";
            this.BtnDownload.Size = new System.Drawing.Size(133, 23);
            this.BtnDownload.TabIndex = 5;
            this.BtnDownload.Text = "Hent Dataset";
            this.BtnDownload.UseVisualStyleBackColor = true;
            this.BtnDownload.Click += new System.EventHandler(this.BtnDownload_Click);
            // 
            // TbActivityLog
            // 
            this.TbActivityLog.Location = new System.Drawing.Point(13, 222);
            this.TbActivityLog.Multiline = true;
            this.TbActivityLog.Name = "TbActivityLog";
            this.TbActivityLog.ReadOnly = true;
            this.TbActivityLog.Size = new System.Drawing.Size(759, 164);
            this.TbActivityLog.TabIndex = 10;
            this.TbActivityLog.TabStop = false;
            this.TbActivityLog.WordWrap = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 204);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(70, 15);
            this.label4.TabIndex = 11;
            this.label4.Text = "Activity Log";
            // 
            // RadioOauth
            // 
            this.RadioOauth.AutoSize = true;
            this.RadioOauth.Checked = true;
            this.RadioOauth.Location = new System.Drawing.Point(547, 17);
            this.RadioOauth.Name = "RadioOauth";
            this.RadioOauth.Size = new System.Drawing.Size(160, 19);
            this.RadioOauth.TabIndex = 6;
            this.RadioOauth.TabStop = true;
            this.RadioOauth.Text = "OAuth 2.0 Authentication";
            this.RadioOauth.UseVisualStyleBackColor = true;
            // 
            // RadioBasicAuth
            // 
            this.RadioBasicAuth.AutoSize = true;
            this.RadioBasicAuth.Location = new System.Drawing.Point(547, 42);
            this.RadioBasicAuth.Name = "RadioBasicAuth";
            this.RadioBasicAuth.Size = new System.Drawing.Size(134, 19);
            this.RadioBasicAuth.TabIndex = 7;
            this.RadioBasicAuth.TabStop = true;
            this.RadioBasicAuth.Text = "Basic Authentication";
            this.RadioBasicAuth.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 416);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.TbActivityLog);
            this.Controls.Add(this.BtnDownload);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.TbDownloadUrl);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "Form1";
            this.Text = "Dataudveksler Download Tool";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox TbPassword;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TbClientId;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox TbDownloadUrl;
        private System.Windows.Forms.Button BtnDownload;
        private System.Windows.Forms.TextBox TbActivityLog;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.RadioButton RadioBasicAuth;
        private System.Windows.Forms.RadioButton RadioOauth;
    }
}

